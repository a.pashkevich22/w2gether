import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    playerLink: '',
    connections: [],
  },
  mutations: {
    setLink(state, link) {
      state.playerLink = link;
    },
    addConnection(state, connection) {
      state.connections.push(connection);
    }
  },
  actions: {
    broadcast({state}, { type, data }) {
      state.connections.forEach(connection => {
        console.log('--->', {type, data});
        connection.send({
          type,
          data,
        })
      })
    }
  }
})
