import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VuePlyr from 'vue-plyr'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import VuePeerJS from 'vue-peerjs';
import Peer from 'peerjs';

Vue.use(VuePeerJS, new Peer({
  host: 'peerjsserver1.herokuapp.com',
  port: location.protocol.includes('s')?443:80,
}))
Vue.use(ElementUI);
Vue.use(VuePlyr, {
  plyr: {
    fullscreen: { enabled: false }
  },
  emit: ['ended']
})


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
